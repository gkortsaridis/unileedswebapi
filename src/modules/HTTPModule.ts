import * as request from "request";
import {superReplace} from "../utilities/Helpers";

export class HTTPModule {

  public httpGet(url: string): Promise<string>{
    return new Promise<string>((resolve, reject) => {
      var options = {
        url:  url,
        timeout: 15*1000,
        pool: {maxSockets: 150}
      }
    
      request(options, (err : any, res: any, body: any) => {
        if (err) {
          //console.log("HTTP MODULE ERROR : "+err);
          reject(err);
        }

        if (res && res.statusCode === 200) {
          let result = superReplace(res.body);
          resolve(result);
        } else {
          reject(JSON.stringify(res));
        }
      });
    });
  }
}
