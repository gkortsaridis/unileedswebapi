import {NewsRepository} from "./repositories/NewsRepository";
import {EventsRepository} from "./repositories/EventsRepository";
import {FacultiesRepository} from "./repositories/FacultiesRepository";
import {TimetableRepository} from "./repositories/TimetableRepository";
import {MongoDBRepository} from "./repositories/MongoDBRepository";
import {TimetableEntry} from "./entities/TimetableEntry";
import {Event} from "./entities/Event";
import {New} from "./entities/New";
import {Faculty} from "./entities/Faculty";
import {Module} from "./entities/Module";
import {openApiDocumentation} from "./utilities/openApiDocumentation";

const mongoDBRepository: MongoDBRepository = new MongoDBRepository();

const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 8080;
app.use(bodyParser.json());

console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
console.log("| Welcome in NextGeneration University of Leeds Web API |")
console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
console.log(" Developer  : Georgios Kortsaridis, SC18GK");
console.log(" Supervisor : Dr Nick Efford");

const args = process.argv.slice(2);
if(args[0] == "webserver"){
  app.post('/timetable', (req:any, res:any) => {
    if(req.body.modules){
      mongoDBRepository.loadTimetablesForModules(req.body.modules).then( (data: Array<TimetableEntry>) => {
        res.set('Content-Type', 'application/json');
        res.send({"timetable":data});
      });
    }else{
      res.send("No Modules array on "+JSON.stringify(req.body));
    }
  });

  app.post('/news_events', (req:any, res:any) => {

    let news: Array<New> | null = null;
    let events: Array<Event> | null = null;

    mongoDBRepository.loadAllEvents().then( (data: Array<Event>) => {
      if(events == null){
        events = data;
        if(news != null){
          res.send({"news": news, "events": events});
        }
      }
    });

    mongoDBRepository.loadAllNews().then( (data: Array<New>) => {
      if(news == null){
        news = data;
        if(events != null){
          res.send({"news": news, "events": events});
        }
      }
    });

  });

  app.post('/faculties',(req:any, res:any) => {
    mongoDBRepository.loadAllFaculties().then( (data: Array<Faculty>) => {
      res.set('Content-Type', 'application/json');
      res.send({"faculties":data});
    });

  });

  app.post('/modules',(req:any, res:any) => {
    mongoDBRepository.loadAllModules().then( (data: Array<Module>) => {
      res.set('Content-Type', 'application/json');
      res.send({"modules":data});
    });
  });

  const swaggerUi = require('swagger-ui-express');
  app.use('/', swaggerUi.serve, swaggerUi.setup(openApiDocumentation));


  app.listen(port, () => console.log(`Server listening on port ${port}!`));
}else if(args[0] == "updateDB"){
  console.log("Database Update");
  mongoDBRepository.updateDatabase().then( () => {
    console.log("-> Databse is up-to-date. Opening HTTP Server for external API requests\n");
  }).catch((reason:any)=>{
    console.log(reason);
  });
}




