import {Module} from "./Module";
import {superReplace} from "../utilities/Helpers";

export class TimetableEntry {
  get weekday(): string {
    return this._weekday;
  }

  set weekday(value: string) {
    this._weekday = value;
  }
  get recordingStatus(): string {
    return this._recordingStatus;
  }

  set recordingStatus(value: string) {
    this._recordingStatus = value;
  }
  get staff(): string {
    return this._staff;
  }

  set staff(value: string) {
    this._staff = value;
  }
  get teachingWeeksSplusWeeks(): string {
    return this._teachingWeeksSplusWeeks;
  }

  set teachingWeeksSplusWeeks(value: string) {
    this._teachingWeeksSplusWeeks = value;
  }
  get teachingWeeksUniWeeks(): string {
    return this._teachingWeeksUniWeeks;
  }

  set teachingWeeksUniWeeks(value: string) {
    this._teachingWeeksUniWeeks = value;
  }
  get finishTime(): string {
    return this._finishTime;
  }

  set finishTime(value: string) {
    this._finishTime = value;
  }
  get startTime(): string {
    return this._startTime;
  }

  set startTime(value: string) {
    this._startTime = value;
  }
  get location(): string {
    return this._location;
  }

  set location(value: string) {
    this._location = value;
  }
  get parentActivity(): string {
    return this._parentActivity;
  }

  set parentActivity(value: string) {
    this._parentActivity = value;
  }
  get activity(): string {
    return this._activity;
  }

  set activity(value: string) {
    this._activity = value;
  }
  get module(): Module {
    return this._module;
  }

  set module(value: Module) {
    this._module = value;
  }

  get isUndergraduate(): boolean {
    return this._isUndergraduate;
  }

  set isUndergraduate(value: boolean){
    this._isUndergraduate = value;
  }

  private _module: Module;
  private _activity: string;
  private _parentActivity: string;
  private _location: string;
  private _weekday: string;
  private _startTime: string;
  private _finishTime: string;
  private _teachingWeeksUniWeeks: string;
  private _teachingWeeksSplusWeeks: string;
  private _staff: string;
  private _recordingStatus: string;
  private _isUndergraduate: boolean;

  constructor(module: Module, activity: string,
              parentActivity: string, location: string,
              weekday: string,
              startTime: string, finishTime: string,
              teachingWeeks1: string, teachingWeeks2: string,
              staff: string, recordingStatus: string, isUndergraduate: boolean) {
    this._module = module;
    this._activity = superReplace(activity);
    this._parentActivity = superReplace(parentActivity);
    this._location = superReplace(location);
    this._weekday = superReplace(weekday);
    this._startTime = superReplace(startTime);
    this._finishTime = superReplace(finishTime);
    this._teachingWeeksUniWeeks = superReplace(teachingWeeks1);
    this._teachingWeeksSplusWeeks = superReplace(teachingWeeks2);
    this._staff = superReplace(staff);
    this._recordingStatus = superReplace(recordingStatus);
    this._isUndergraduate = isUndergraduate;
  }

  public toString(){
    return "Module: "+this._module.name+", Activity  : "+this._activity+", Date: "+this._weekday+", "+this._startTime+"-"+this._finishTime;
  }

}
