import {superReplace} from "../utilities/Helpers";

export class Module {
  get isUndergraduate(): boolean {
    return this._isUndergraduate;
  }

  set isUndergraduate(value: boolean) {
    this._isUndergraduate = value;
  }
  get schoolID(): string {
    return this._schoolID;
  }

  set schoolID(value: string) {
    this._schoolID = value;
  }
  get id(): string {
    return this._moduleID;
  }

  set id(value: string) {
    this._moduleID = value;
  }
  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
  get isDiscoveryModule(): boolean {
    return this._isDiscoveryModule;
  }

  set isDiscoveryModule(value: boolean) {
    this._isDiscoveryModule = value;
  }
  get isAvailableForIncomingStudyAbroad(): boolean {
    return this._isAvailableForIncomingStudyAbroad;
  }

  set isAvailableForIncomingStudyAbroad(value: boolean) {
    this._isAvailableForIncomingStudyAbroad = value;
  }

  private _moduleID: string;
  private _name: string;
  private _isDiscoveryModule: boolean;
  private _isAvailableForIncomingStudyAbroad: boolean;
  private _schoolID: string;
  private _isUndergraduate: boolean;

  constructor(schoolID: string, id: string, name: string, d: boolean, isa: boolean, isUndergraduate: boolean) {
    this._schoolID = superReplace(schoolID);
    this._moduleID = superReplace(id);
    this._name = superReplace(name);
    this._isAvailableForIncomingStudyAbroad = isa;
    this._isDiscoveryModule = d;
    this._isUndergraduate = isUndergraduate;
  }

  public toString():string{
    return this._moduleID+"->"+this._name;
  }
}
