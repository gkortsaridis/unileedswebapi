import {superReplace} from "../utilities/Helpers";

export class KeyValueUrl {
  get url(): string {
    return this._url;
  }

  set url(value: string) {
    this._url = value;
  }
  get value(): string {
    return this._value;
  }

  set value(value: string) {
    this._value = value;
  }
  get key(): string {
    return this._key;
  }

  set key(value: string) {
    this._key = value;
  }

  private _key: string;
  private _value: string;
  private _url: string;

  constructor(key: string, value: string, url: string) {
    this._key = superReplace(key);
    this._value = superReplace(value);
    this._url = superReplace(url);
  }

  public toString(){
    return "\nKey: "+this._key+", Value  : "+this._value+", URL: "+this._url+"\n";
  }

}
