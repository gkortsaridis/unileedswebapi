import {KeyValueUrl} from "./KeyValueUrl";
import {superReplace} from "../utilities/Helpers";

export class Faculty {
  get info(): Array<KeyValueUrl> {
    return this._info;
  }

  set info(value: Array<KeyValueUrl>) {
    this._info = value;
  }
  get website(): string {
    return this._website;
  }

  set website(value: string) {
    this._website = value;
  }
  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  private _name: string;
  private _website: string;
  private _info: Array<KeyValueUrl>

  constructor(name: string, website: string, info: Array<KeyValueUrl>){
    this._name = superReplace(name);
    this._info = info;
    this._website = superReplace(website);
  }


  public toString() {
    return(
      "~~~~~~~~~~Faculty~~~~~~~~~~~~~~~~~~~"+"\n" +
      "Name    : "+this._name+"\n" +
      "Website : "+this._website+"\n" +
      "Info    : "+this._info+"\n" +
      "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+"\n"
    );
  }


}
