import {superReplace} from "../utilities/Helpers";

export class Event {
  get link_url(): string {
    return this._link_url;
  }

  set link_url(value: string) {
    this._link_url = value;
  }
  get img_url(): string {
    return this._img_url;
  }

  set img_url(value: string) {
    this._img_url = value;
  }
  get datetime(): string {
    return this._datetime;
  }

  set datetime(value: string) {
    this._datetime = value;
  }
  get category(): string {
    return this._category;
  }

  set category(value: string) {
    this._category = value;
  }
  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  public toString() {
    return(
      "~~~~~~~~~~EVENT~~~~~~~~~~~~~~~~~~~"+"\n" +
      "Title : "+this._title+"\n" +
      "Category : "+this._category+"\n" +
      "Datetime : "+this._datetime+"\n" +
      "IMG URL  : "+this._img_url+"\n" +
      "LINK URL : "+this._link_url+"\n"+
      "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"+"\n"
    );
  }

  private _title: string;
  private _category: string;
  private _datetime: string;
  private _img_url: string;
  private _link_url: string;

  constructor(title: string, category: string, datetime: string, img_url: string, link_url: string) {
    this._title = superReplace(title);
    this._category = superReplace(category);
    this._datetime = superReplace(datetime);
    this._img_url = superReplace(img_url);
    this._link_url = superReplace(link_url);
  }

}
