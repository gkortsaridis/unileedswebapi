export function superTrim(str: string | null): string{
  if(str){
    return str.toString()
        .replace(new RegExp("\n", 'g'),"")
        .replace(new RegExp("\t", 'g'),"")
        .trim();
  }else{
    return '';
  }
}

export function superReplace(str: string | null): string{
  if(str){
    return str.toString()
      .replace(new RegExp("&nbsp;", 'g'),"")
      .replace(new RegExp("&lt;", 'g'),"<")
      .replace(new RegExp("&gt;", 'g'),">")
      .replace(new RegExp("&amp;", 'g'),"&")
      .replace(new RegExp("&quot;", 'g'),"\"")
      .replace(new RegExp("&apos;", 'g'),"'")
      .replace(new RegExp("&cent;", 'g'),"¢")
      .replace(new RegExp("&pound;", 'g'),"£")
      .replace(new RegExp("&yen;", 'g'),"¥")
      .replace(new RegExp("&euro;", 'g'),"€")
      .replace(new RegExp("&copy;", 'g'),"©")
      .replace(new RegExp("&reg;", 'g'),"®")
      .trim();
  }else{
    return '';
  }
}

export function executeAllPromises(promises: any) {
  // Wrap all Promises in a Promise that will always "resolve"
  var resolvingPromises = promises.map(function(promise: any) {
    return new Promise(function(resolve) {
      let payload = new Array(2);
      promise.then(function(result: any) {
        payload[0] = result;
      })
        .catch(function(error: any) {
          payload[1] = error;
        })
        .then(function() {
          /*
           * The wrapped Promise returns an array:
           * The first position in the array holds the result (if any)
           * The second position in the array holds the error (if any)
           */
          resolve(payload);
        });
    });
  });

  let errors: any = [];
  let results: any = [];

  // Execute all wrapped Promises
  return Promise.all(resolvingPromises)
    .then(function(items) {
      items.forEach(function(payload: any) {
        if (payload[1]) {
          errors.push(payload[1]);
        } else {
          results.push(payload[0]);
        }
      });

      return {
        errors: errors,
        results: results
      };
    });
}

export const schools = ["BIOC", "BIOL", "BLGY", "MEDP", "BISC", "LUBS", "CSER", "PREN", "CHEM", "CIVE", "COLO", "COMP",
  "DENT", "DESN", "ODLC", "SOEE", "EDUC", "ELEC", "ENGL", "EPIB", "ARTF", "FOOD", "FOUN", "GEOG",
  "HECS", "HIST", "IDEA", "JEST", "ELU",  "AMES", "CLAS", "EAST", "FREN", "GERM", "ITAL", "SMLC",
  "FLTU", "LING", "MODL", "RUSL", "SPPO", "LAW",  "LIHS", "LEED", "LLLC", "MATH", "MECH", "COMM",
  "MEDS", "MEDV", "MICR", "CMNS", "MUSC", "PACI", "PRHS", "PHAS", "PIED", "GPPH", "PSIA", "PSYC",
  "SLSP", "SPSC", "SABD", "TRAN"];

export function printProgress(progress: string){
  var readline = require('readline');

  //readline.clearLine(process.stdout);
  readline.cursorTo(process.stdout, 0);
  process.stdout.write(progress);

  //process.stdout.clearLine();
  //process.stdout.cursorTo(0);
  //rocess.stdout.write(progress + '%');
}
