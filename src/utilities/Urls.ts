export const uniLeedsBaseURL = "https://www.leeds.ac.uk/";
export const uniLeedsFacultiesURL = "http://www.leeds.ac.uk/info/130500/faculties";
export const uniLeedsTimetablesURL = "http://timetable.leeds.ac.uk/teaching/201819/module_timetablers.htm";

export function moduleTimetableUrl(module: string){
  return "http://timetable.leeds.ac.uk/teaching/201819/reporting/Textspreadsheet?objectclass=module&idtype=name&identifier="+module+"&template=SWSCUSTtimetablers+module+individual&days=1-7&periods=1-21&weeks=1-52"
}


export function undergraduatesModulesListUrl(schoolId: string){
  return "http://webprod3.leeds.ac.uk/catalogue/modulesearch.asp?L=UG&Y=201920&E="+schoolId+"&N=all&S=&A=any"
}

export function postgraduateModulesListUrl(schoolId: string){
  return "http://webprod3.leeds.ac.uk/catalogue/modulesearch.asp?L=TP&Y=201920&E="+schoolId+"&N=all&S=&A=any"
}
