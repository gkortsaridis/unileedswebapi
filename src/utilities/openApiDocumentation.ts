export const openApiDocumentation = {
          openapi: '3.0.1',
          info: {
              version: '1.0.0',
              title: 'University of Leeds OpenData BackEnd',
              description: 'Open University data API',
              contact: {
                  name: 'Georgios Kortsaridis',
                  email: 'gkortsaridis@gmail.com',
              },
              license: {
                  name: 'Apache 2.0',
                  url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
              }
          },
          servers: [
              {
                  url: 'http://localhost:8888/',
                  description: 'Local server'
              },
              {
                  url: 'http://52.56.59.182:8888/',
                  description: 'AWS server'
              }
          ],
          paths: {
              '/news_events': {
                  get: {
                      tags: ['CRUD operations'],
                      description: 'Get the latest University of Leeds News & Events posted on the website',
                      responses: {
                          '200': {
                              description: 'News & Events successfully obtained',
                              content: {
                                  'application/json': {
                                      schema: {
                                          $ref: '#/components/schemas/NewsEventsResponse'
                                      }
                                  }
                              }
                          }
                      }
                  }
              },
              '/timetable':{
                  post: {
                      tags: ['CRUD operations'],
                      description: 'Timetable for specific modules',
                      parameters: [],
                      requestBody: {
                          content: {
                              'application/json': {
                                  schema: {
                                      $ref: '#/components/schemas/TimetableExample'
                                  }
                              }
                          },
                          required: true
                      },
                      responses: {
                          '200': {
                              description: 'Timetables for Modules selected',
                              content: {
                                  'application/json': {
                                      schema: {
                                          $ref: '#/components/schemas/TimetableResponse'
                                      }
                                  }
                              }
                          }
                      }
                  }
              },
              '/faculties':{
                  get: {
                      tags: ['CRUD operations'],
                      description: 'Get University of Leeds Faculties information',
                      responses: {
                          '200': {
                              description: 'Faculties successfully obtained',
                              content: {
                                  'application/json': {
                                      schema: {
                                          $ref: '#/components/schemas/FacultiesResponse'
                                      }
                                  }
                              }
                          }
                      }
                  }
              },
              '/modules':{
                  get: {
                      tags: ['CRUD operations'],
                      description: 'Get University of Leeds Modules information',
                      responses: {
                          '200': {
                              description: 'Modules successfully obtained',
                              content: {
                                  'application/json': {
                                      schema: {
                                          $ref: '#/components/schemas/ModuleResponse'
                                      }
                                  }
                              }
                          }
                      }
                  }
              },
          },
          components: {
              schemas: {
                  _title: {
                      type: 'string',
                      example: 'Flash News in University of Leeds'
                  },
                  _category: {
                      type: 'string',
                      example: 'Entertainment'
                  },
                  _datetime: {
                      type: 'string',
                      example: '2019-07-25'
                  },
                  _img_url: {
                      type: 'string',
                      example: 'www.leeds.ac.uk/images/Weather_radar_web.jpg'
                  },
                  _link_url: {
                      type: 'string',
                      example: 'www.leeds.ac.uk/news/article/4451/using_weather_radar_to_monitor_insects'
                  },
                  UniversityNewEvent: {
                      type: 'object',
                      properties: {
                          _title: {
                              $ref: '#/components/schemas/_title'
                          },
                          _datetime: {
                              $ref: '#/components/schemas/_datetime'
                          },
                          _img_url: {
                              $ref: '#/components/schemas/_img_url'
                          },
                          _link_url: {
                              $ref: '#/components/schemas/_link_url'
                          }
                      }
                  },
                  NewsEventsResponse: {
                      type: 'object',
                      properties: {
                          news: {
                              type: 'array',
                              items: {
                                  $ref: '#/components/schemas/UniversityNewEvent'
                              }
                          },
                          events: {
                              type: 'array',
                              items: {
                                  $ref: '#/components/schemas/UniversityNewEvent'
                              }
                          }
                      }
                  },

                  ModuleString:{
                      type: 'string',
                      example: 'COMP3222'
                  },
                  TimetableExample:{
                    type: 'object',
                    properties: {
                        modules: {
                            type: 'array',
                            items:{
                                type: 'string',
                                example: 'COMP3222'
                            }
                        }
                    }
                  },

                  _name: {
                      type: 'string',
                      example: 'Faculty of Engineering'
                  },
                  _key: {
                      type: 'string',
                      example: 'Executive Dean'
                  },
                  _value: {
                      type: 'string',
                      example: 'Professor Peter Jimack'
                  },
                  _url: {
                      type: 'string',
                      example: 'http://www.engineering.leeds.ac.uk/people/computing/staff/p.k.jimack'
                  },
                  _info: {
                      type: 'object',
                      properties: {
                          _key: {
                              $ref: '#/components/schemas/_key'
                          },
                          _value: {
                              $ref: '#/components/schemas/_value'
                          },
                          _url: {
                              $ref: '#/components/schemas/_url'
                          },
                      }
                  },
                  Faculty:{
                      type: 'object',
                      properties: {
                          _name: {
                              $ref: '#/components/schemas/_name'
                          },
                          _info: {
                              type: 'array',
                              items:{
                                  $ref: '#/components/schemas/_info'
                              }
                          }
                      }
                  },
                  FacultiesResponse: {
                      type: 'object',
                      properties: {
                          faculties: {
                              type: 'array',
                              items:{
                                  $ref: '#/components/schemas/Faculty'
                              }
                          }
                      }
                  },
                  Module: {
                      type: 'object',
                      properties: {
                          _schoolID: {
                              type: 'string',
                              example: 'COMP'
                          },
                          _moduleID: {
                              type: 'string',
                              example: 'COMP3222'
                          },
                          _name: {
                              type: 'string',
                              example: 'Mobile Application Development'
                          },
                          _isAvailableForIncomingStudyAbroad: {
                              type: 'boolean',
                              example: true
                          },
                          _isDiscoveryModule: {
                              type: 'boolean',
                              example: false
                          },
                          _isUndergraduate: {
                              type: 'boolean',
                              example: true
                          },
                      }
                  },
                  ModuleResponse:{
                      type: 'object',
                      properties: {
                          modules: {
                              type: 'array',
                              items:{
                                  $ref: '#/components/schemas/Module'
                              }
                          }
                      }
                  },

                  Timetable: {
                      type: 'object',
                      properties: {
                          module: {
                              $ref: '#/components/schemas/Module'
                          },
                          _activity:{
                              type: 'string',
                              example: 'COMP322201/LEC 1/01'
                          },
                          _parentActivity:{
                              type: 'string',
                              example: ''
                          },
                          _location:{
                              type: 'string',
                              example: 'Roger Stevens LT 16 (12.16)'
                          },
                          _weekday:{
                              type: 'string',
                              example: 'Tuesday'
                          },
                          _startTime:{
                              type: 'string',
                              example: '13:00'
                          },
                          _finishTime:{
                              type: 'string',
                              example: '14.00'
                          },
                          _teachingWeeksUniWeeks:{
                              type: 'string',
                              example: '14-22, 23-24'
                          },
                          _teachingWeeksSplusWeeks:{
                              type: 'string',
                              example: ''
                          },
                          _staff:{
                              type: 'string',
                              example: 'Efford,Nicholas,Dr'
                          },
                          _recordingStatus:{
                              type: 'integer',
                              example: 3
                          },
                          _isUndergraduate:{
                              type: 'boolean',
                              example: true
                          }
                      }
                  },
                  TimetableResponse: {
                      type: 'object',
                      properties: {
                          timetables: {
                              type: 'array',
                              items: {
                                  $ref: '#/components/schemas/Timetable'
                              }
                          }
                      }
                  }
              },
          }
      };
