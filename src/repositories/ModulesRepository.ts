import {HTTPModule} from "../modules/HTTPModule";
import {Module} from "../entities/Module";
import {postgraduateModulesListUrl, undergraduatesModulesListUrl} from "../utilities/Urls";
import {DOMWindow, JSDOM} from "jsdom";
import {executeAllPromises, printProgress} from "../utilities/Helpers";
import {TimetableEntry} from "../entities/TimetableEntry";

export class ModulesRepository {

  private httpModule: HTTPModule;

  constructor(){
    this.httpModule = new HTTPModule();
  }

  public getUndergraduateModulesList(schoolID: string): Promise<Array<Module>> {
    return new Promise<Array<Module>>((resolve, reject) => {

      this.httpModule.httpGet(undergraduatesModulesListUrl(schoolID))
        .then((result : string) => {

          let modules = Array<Module>();

          const dom = new JSDOM(result);
          const body = dom.window.document.querySelector("body");
          const tables = body && body.querySelectorAll("table");

          if(tables){
           for(let i=0; i<tables.length; i++) {
             const modulesTable = tables[i];

             if(this.isModuleTable(modulesTable)){
               const trs = modulesTable.querySelectorAll("tr");
               for(let j=0; j<trs.length; j++){
                 const moduleObj = trs[j];
                 const tds = moduleObj.querySelectorAll("td");
                 const idObj = tds[2].querySelector("a");
                 if(idObj != null){
                   const module: Module = new Module(
                     schoolID,
                     idObj.innerHTML,
                     tds[3].innerHTML,
                     tds[1].innerHTML == "d",
                     tds[0].innerHTML == "isa",
                     true);
                   modules.push(module);
                 }
               }
             }
           }
          }

          resolve(modules);

        })
        .catch((error: any) => {
          reject(error);
        });
    });
  }

  public getPostgraduateModulesList(schoolID: string): Promise<Array<Module>> {
    return new Promise<Array<Module>>((resolve, reject) => {
      this.httpModule.httpGet(postgraduateModulesListUrl(schoolID))
        .then((result : string) => {

          let modules = Array<Module>();

          const dom = new JSDOM(result);
          const body = dom.window.document.querySelector("body");
          const tables = body && body.querySelectorAll("table");

          if(tables){
            for(let i=0; i<tables.length; i++) {
              const modulesTable = tables[i];

              if(this.isModuleTable(modulesTable)){
                const trs = modulesTable.querySelectorAll("tr");
                for(let j=0; j<trs.length; j++){
                  const moduleObj = trs[j];
                  const tds = moduleObj.querySelectorAll("td");
                  const idObj = tds[2].querySelector("a");
                  if(idObj != null){
                    const module: Module = new Module(
                      schoolID,
                      idObj.innerHTML,
                      tds[3].innerHTML,
                      tds[1].innerHTML == "d",
                      tds[0].innerHTML == "isa",
                      false);
                    modules.push(module);
                  }
                }
              }
            }
          }

          resolve(modules);

        }).catch((error: any) => {
          reject(error);
        });
    });
  }

  public getAllUniversityModuleList(schoolIDs: Array<string>): Promise<Array<Module>>{
    return new Promise<Array<Module>>((bigResolve, reject) => {

      let allUniversityModules: Array<Module> = [];

      for (let i = 0, p = Promise.resolve(); i < schoolIDs.length; i++) {
        p = p.then(_ => new Promise(resolve => {

          this.getUndergraduateModulesList(schoolIDs[i]).then( (result: Array<Module>) => {
            for(let i=0; i<result.length; i++){
              allUniversityModules.push(result[i]);
            }
            this.getPostgraduateModulesList(schoolIDs[i]).then( (result: Array<Module>) => {

              for(let i=0; i<result.length; i++) {
                allUniversityModules.push(result[i]);
              }
              printProgress("...Crawled "+(i+1)+"/"+schoolIDs.length+" schools, found "+allUniversityModules.length+" modules");


              if(i == schoolIDs.length -1){
                bigResolve(allUniversityModules);
              }else{
                resolve();
              }
            });
          }).catch( (error: any) => {
            console.log(error);
            resolve(error);
          });
        }));
      }
    });
  }

  private isModuleTable(table: HTMLElement): boolean{
    const tr = table.querySelector("tr");
    const tds = tr && tr.querySelectorAll("td");

    return (tds && tds.length == 4) ? true : false;
  }

}
