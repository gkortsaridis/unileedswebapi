import {HTTPModule} from "../modules/HTTPModule";
//import {uniLeedsTimetablesURL} from "../utilities/Urls";
import {JSDOM} from "jsdom";
import {TimetableEntry} from "../entities/TimetableEntry";
import {moduleTimetableUrl} from "../utilities/Urls";
import {Module} from "../entities/Module";
import {ModulesRepository} from "./ModulesRepository";
import {executeAllPromises, printProgress, schools} from "../utilities/Helpers";

export class TimetableRepository{

  private httpModule: HTTPModule;

  constructor(){
    this.httpModule = new HTTPModule();
  }

  public getTimetable(module: Module): Promise<Array<TimetableEntry>>{
    return new Promise<Array<TimetableEntry>>((resolve, reject) => {

      let timetables : Array<TimetableEntry> = [];
      const days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

      //Aparently we need to add the "01" to the end of each module to get the timetable
      const moduleId =module.id+"01";
      this.httpModule.httpGet(moduleTimetableUrl(moduleId))
        .then((result : string) => {

          const dom = new JSDOM(result);
          const body = dom.window.document.querySelector("body");
          const tables = body && body.querySelectorAll("table.spreadsheet");

          if(tables){
            for(let j=0; j<tables.length; j++){
              const table = tables[j];
              const tbody = table.querySelector("tbody");
              if(tbody){

                const trs = tbody && tbody.querySelectorAll("tr");
                if(trs && trs.length > 1){

                  for(let i=1; i<trs.length; i++){
                    const tds = trs[i].querySelectorAll("td");

                    var location = tds[2].innerHTML;
                    const locDiv = tds[2].querySelector("div");
                    if(locDiv != null){
                      location = locDiv.innerHTML;

                      const locObj = locDiv.querySelector("a");
                      if(locObj != null){
                        location = locObj.innerHTML;
                      }
                    }


                    const timetableEntry = new TimetableEntry(
                      module,
                      tds[0].innerHTML,
                      tds[1].innerHTML,
                      location,
                      days[j],
                      tds[10].innerHTML,
                      tds[11].innerHTML,
                      tds[12].innerHTML,
                      tds[13].innerHTML,
                      tds[17].innerHTML,
                      tds[18].innerHTML,
                      true);

                    timetables.push(timetableEntry);
                  }

                }
              }
            }
          }
          resolve(timetables);

        }).catch((error: any) => {
          try{
            let errorJson = JSON.parse(error);
            reject("Module :"+module+" -> Error:"+errorJson.statusCode);
          }catch(error){
            reject("Module :"+module+" -> Error:"+error);
          }
          
      });
    });

  }

  public getAllUniversityTimetables(modules: Array<Module>): Promise<Array<TimetableEntry>>{
    return new Promise<Array<TimetableEntry>>( (bigResolve, bigReject) => {

      //Repositories Needed
      const timetableRepository = new TimetableRepository();
      const moduleRepository = new ModulesRepository();

      const modulesBatch = 1;

        const batchTimes = Math.floor(modules.length/modulesBatch);
        let allUniversityTimetables: Array<TimetableEntry> = [];

        for (let i = 0, p = Promise.resolve(); i < batchTimes; i++) {
          p = p.then(_ => new Promise(resolve => {

            timetableRepository.getTimetable(modules[i]).then( (timetables: Array<TimetableEntry>) => {
              printProgress("Timetables promise : "+i+"/"+batchTimes+" -> "+allUniversityTimetables.length+" total timetables");

              for(let j=0; j<timetables.length; j++) {
                allUniversityTimetables.push(timetables[j]);
              }
              resolve();
            }).catch( (error: any) => {
              printProgress("Timetable retrieval error: "+error+"\n");
              resolve(error);
            });

            if(i == batchTimes-1){
              bigResolve(allUniversityTimetables);
            }
          }));
        }
    });
  }


}
