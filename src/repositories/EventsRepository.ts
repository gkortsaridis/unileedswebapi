import {HTTPModule} from "../modules/HTTPModule";
import {JSDOM} from "jsdom";
import {Event} from "../entities/Event";
import {superReplace, superTrim} from "../utilities/Helpers";
import {uniLeedsBaseURL} from "../utilities/Urls";

export class EventsRepository {

  private httpModule: HTTPModule;

  constructor(){
    this.httpModule = new HTTPModule();
  }

  public retrieveEvents(): Promise<Array<Event>>{
    return new Promise<Array<Event>>((resolve, reject) => {

      this.httpModule.httpGet(uniLeedsBaseURL)
        .then((result : string) => {
          this.parseEvents(result)
            .then((newsArr: Array<Event>) => {
              resolve(newsArr);
            }).catch((error: any) => {
            reject(error);
          });
        }).catch((error: any) => {
        reject(error);
      });
    });

  }

  private parseEvents(resp: string): Promise<Array<Event>>{
    return new Promise<Array<Event>>((resolve, reject) => {

      const dom = new JSDOM(resp);
      const newsRoot = dom.window.document.querySelector("div.news-grid__body");
      const tabEvents = newsRoot && newsRoot.querySelector("#tabEvents");
      const ajaxEvents = tabEvents && tabEvents.querySelector("#ajaxEvents");
      const events = ajaxEvents && ajaxEvents.querySelectorAll(".grid__item");

      let eventsArr : Array<Event> = [];

      if(events){
        for(let i=0; i<events.length; i++){
          const events_item = events[i];

          const square = events_item.querySelector(".square");
          const span = square && square.querySelector("span");
          let IMG_URL = span && span.getAttribute("style");
          if(IMG_URL){
            IMG_URL = IMG_URL.replace("background-image: url('//","");
            IMG_URL = IMG_URL.replace("');","");
          }
          const square_inner = square && square.querySelector(".square__inner");
          const content = square_inner && square_inner.querySelector(".square__inner__content");
          const a = square_inner && square_inner.querySelector("a");
          let URL = a && a.getAttribute("href");
          if(URL){
            URL = URL.replace("//","");
          }

          const details = content && content.querySelector(".square-details");
          const CATEGORY = details && details.querySelector("span");
          const DATETIME = details && details.querySelector("time");
          const TITLE = content && content.querySelector("h3");

          const eventItem = new Event(
            TITLE && TITLE.innerHTML || '',
            superTrim(CATEGORY && CATEGORY.innerHTML) || '',
            superTrim(DATETIME && DATETIME.getAttribute("datetime")) || '',
            superTrim(IMG_URL) || '',
            superTrim(URL) || ''
          );

          eventsArr.push(eventItem);
        }

        resolve(eventsArr);
      }else{
        reject('Events is NULL');
      }

  });
  }

}
