import {Collection, MongoClient} from "mongodb";
import {TimetableEntry} from "../entities/TimetableEntry";
import {Module} from "../entities/Module";
import {Event} from "../entities/Event";
import {New} from "../entities/New";
import {Faculty} from "../entities/Faculty";
import {KeyValueUrl} from "../entities/KeyValueUrl";
import {rejects} from "assert";
import {TimetableRepository} from "./TimetableRepository";
import {NewsRepository} from "./NewsRepository";
import {EventsRepository} from "./EventsRepository";
import {FacultiesRepository} from "./FacultiesRepository";
import {ModulesRepository} from "./ModulesRepository";
import {schools} from "../utilities/Helpers";

export class MongoDBRepository {

  private mongoUrl = "mongodb://localhost:27017";
  private mongoDatabaseName = "NextGenUniLeedsServices";

  //Collections
  private timetablesCollection = "Timetables";
  private eventsCollection = "Events";
  private newsCollection = "News";
  private facultiesCollection = "Faculties";
  private modulesCollection = "Modules";

  private mongo = require('mongodb').MongoClient;

  private timetableRepository: TimetableRepository = new TimetableRepository();
  private newsRepository: NewsRepository = new NewsRepository();
  private eventsRepository: EventsRepository = new EventsRepository();
  private facultiesRepository: FacultiesRepository = new FacultiesRepository();
  private moduleRepository: ModulesRepository = new ModulesRepository();

  private mongoConnect():Promise<MongoClient>{
    return this.mongo.connect(this.mongoUrl, {useNewUrlParser: true});
  }

  private mongoGetCollection(client: MongoClient, collection: string):Collection {
    const db = client.db(this.mongoDatabaseName);
    return db.collection(collection);
  }

  private readCollection(collection: Collection): Promise<any[]> {
    return collection.find().toArray();
  }

  private closeMongo(client: MongoClient) {
    return client.close()
  }

  public saveAllFaculties(faculties: Array<Faculty>):Promise<any>{
    return new Promise<any>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.facultiesCollection);
        collection.deleteMany( {}).then( () => {
          collection.insertMany(faculties).then( (result: any) => {
            this.closeMongo(client);
            resolve(result);
          }).catch( (error: any) => {
            this.closeMongo(client);
            reject(error);
          });
        });
      });
    });
  }

  public loadAllFaculties():Promise<Array<Faculty>> {
    return new Promise<Array<Faculty>>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.facultiesCollection);
        collection.find<New>().toArray().then( (data: any[]) => {
          let finalData: Array<Faculty> = [];

          for (let i=0; i<data.length; i++){
            let d = data[i];
            let n = d._name;
            let w = d._website;
            let info = d._info;
            let infoArr: Array<KeyValueUrl> = [];
            for(let j=0; j<info.length; j++){
              let inf = info[j];
              let k = inf._key;
              let v = inf._value;
              let u = inf._url;
              let kvu = new KeyValueUrl(k,v,u);
              infoArr.push(kvu);
            }
            let faculty = new Faculty(n,w,infoArr);
            finalData.push(faculty);
          }

          this.closeMongo(client);
          resolve(finalData);
        });
      });
    });
  }

  public saveAllNews(news: Array<New>):Promise<any>{
    return new Promise<any>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.newsCollection);
        collection.deleteMany( {}).then( () => {
          collection.insertMany(news).then( (result: any) => {
            this.closeMongo(client);
            resolve(result);
          }).catch( (error: any) => {
            this.closeMongo(client);
            reject(error);
          });
        });
      });
    });

  }

  public loadAllNews():Promise<Array<New>> {
    return new Promise<Array<New>>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.newsCollection);
        collection.find<New>().toArray().then( (data: any[]) => {
          let finalData: Array<New> = [];

          for (let i=0; i<data.length; i++){
            let d = data[i];
            let t = d._title;
            let c = d._category;
            let dt = d._datetime;
            let img = d._img_url;
            let link = d._link_url;
            let _new = new New(t,c,dt,img,link);
            finalData.push(_new);
          }

          this.closeMongo(client);
          resolve(finalData);
        });
      });
    });
  }

  public saveAllEvents(events: Array<Event>):Promise<any>{
    return new Promise<any>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.eventsCollection);
        collection.deleteMany( {}).then( () => {
          collection.insertMany(events).then( (result: any) => {
            this.closeMongo(client);
            resolve(result);
          }).catch( (error: any) => {
            this.closeMongo(client);
            reject(error);
          });
        });
      });
    });

  }

  public loadAllEvents():Promise<Array<Event>> {
    return new Promise<Array<Event>>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.eventsCollection);
        collection.find<Event>().toArray().then( (data: any[]) => {
          let finalData: Array<Event> = [];

          for (let i=0; i<data.length; i++){
            let d = data[i];
            let t = d._title;
            let c = d._category;
            let dt = d._datetime;
            let img = d._img_url;
            let link = d._link_url;
            let event = new Event(t,c,dt,img,link);
            finalData.push(event);
          }

          this.closeMongo(client);
          resolve(finalData);
        });
      });
    });
  }

  public saveAllTimetables(timetables: Array<TimetableEntry>):Promise<any>{
    return new Promise<any>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.timetablesCollection);
        collection.deleteMany( {}).then( () => {
          collection.insertMany(timetables).then( (result: any) => {
            this.closeMongo(client);
            resolve(result);
          }).catch( (error: any) => {
            this.closeMongo(client);
            reject(error);
          });
        });
      });
    });

  }

  public loadAllTimetables():Promise<Array<TimetableEntry>> {
    return new Promise<Array<TimetableEntry>>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.timetablesCollection);
        collection.find<TimetableEntry>().toArray().then( (data: any[]) => {
          let finalData: Array<TimetableEntry> = [];

          for (let i=0; i<data.length; i++){
            let d = data[i];
            let m = d._module;
            let module = new Module(m._schoolID, m._id, m._name, m._isDiscoveryModule, m._isAvailableForIncomingStudyAbroad, m._isUndergraduate);
            let timetable = new TimetableEntry(module, d._activity, d._parentActivity, d._location, d._weekday,
              d._startTime, d._finishTime, d._teachingWeeksUniWeeks, d._isAvailableForIncomingStudyAbroad, d._staff, d._recordingStatus, true);
            finalData.push(timetable);
          }

          this.closeMongo(client);
          resolve(finalData);
        });
      });
    });
  }

  public loadTimetablesForModules(modules: Array<string>):Promise<Array<TimetableEntry>>{
    return new Promise<Array<TimetableEntry>>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.timetablesCollection);

        collection.find({
          "_module._moduleID": {
            $in: modules
          }
        }).toArray().then( (data: any[]) => {
          let finalData: Array<TimetableEntry> = [];

          for (let i=0; i<data.length; i++){
            let d = data[i];
            let m = d._module;
            let module = new Module(m._schoolID, m._moduleID, m._name, m._isDiscoveryModule, m._isAvailableForIncomingStudyAbroad, m._isUndergaduate);
            let timetable = new TimetableEntry(module, d._activity, d._parentActivity, d._location, d._weekday,
              d._startTime, d._finishTime, d._teachingWeeksUniWeeks, d._isAvailableForIncomingStudyAbroad, d._staff, d._recordingStatus, true);
            finalData.push(timetable);
          }

          this.closeMongo(client);
          resolve(finalData);
        });
      });
    });
  }

  public saveAllModules(modules: Array<Module>):Promise<any>{
    return new Promise<any>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.modulesCollection);
        collection.deleteMany( {}).then( () => {
          collection.insertMany(modules).then( (result: any) => {
            this.closeMongo(client);
            resolve(result);
          }).catch( (error: any) => {
            this.closeMongo(client);
            reject(error);
          });
        });
      });
    });

  }

  public loadAllModules():Promise<Array<Module>> {
    return new Promise<Array<Module>>( (resolve, reject) => {
      this.mongoConnect().then( (client: MongoClient) => {
        const collection = this.mongoGetCollection(client, this.modulesCollection);
        collection.find<Module>().toArray().then( (data: any[]) => {
          let finalData: Array<Module> = [];

          for (let i=0; i<data.length; i++){
            let d = data[i];
            let module = new Module(d._schoolID, d._moduleID, d._name, d._isDiscoveryModule, d._isAvailableForIncomingStudyAbroad, d._isUndergraduate);
            finalData.push(module);
          }

          this.closeMongo(client);
          resolve(finalData);
        });
      });
    });
  }



  public updateDatabase(): Promise<any>{
    return new Promise<any>((resolve, reject) => {

      this.eventsRepository.retrieveEvents().then((eventArr: Array<Event>) => {
        this.saveAllEvents(eventArr).then( (result: any) => {
          console.log("Updated Events: " +eventArr.length);

          this.newsRepository.retrieveNews().then( (newsArr: Array<New>) => {
            this.saveAllNews(newsArr).then((result: any) =>{
              console.log("Updated News: "+newsArr.length);

              this.facultiesRepository.retrieveFaculties().then( (facultiesArr: Array<Faculty>) => {
                this.saveAllFaculties(facultiesArr).then( (result: any) => {
                  console.log("Updated Faculties: "+facultiesArr.length);

                  this.moduleRepository.getAllUniversityModuleList(schools).then( (modules: Array<Module>) => {
                    console.log("Received modules "+modules.length);
                    this.saveAllModules(modules).then( (result: any) => {
                      console.log("Updated Modules: "+modules.length);

                      this.timetableRepository.getAllUniversityTimetables(modules).then( (timetablesArr: Array<TimetableEntry>) => {
                        this.saveAllTimetables(timetablesArr).then( (result: any) => {
                          console.log("Updated Timetables: "+timetablesArr.length);
                          resolve("Updated Database at "+new Date().toISOString());

                        }).catch((reason:any) => { reject("Timetables Saving Error "+reason);  });
                      }).catch((reason:any) => { reject("Timetables retrieving Error "+reason); });
                    }).catch((reason:any) => { reject("Modules Saving Error "+reason); });
                  }).catch((reason: any) => { reject("Modules retrieving Error "+reason); });
                }).catch((reason:any) => { reject("Facultis Saving Error "+reason); });
              }).catch((reason:any) => { reject("Faculties retrieving Error "+reason); });
            }).catch((reason:any) => { reject("News Saving Error "+reason); });
          }).catch((reason:any) => { reject("News retrieving Error "+reason); });
        }).catch((reason:any) => { reject("Events Saving Error "+reason); });
      }).catch((reason:any)=>{ reject("Events retrieving Error "+reason); });
    });
  }


}
