import {Faculty} from "../entities/Faculty";
import { uniLeedsFacultiesURL} from "../utilities/Urls";
import {HTTPModule} from "../modules/HTTPModule";
import {JSDOM} from "jsdom";
import {KeyValueUrl} from "../entities/KeyValueUrl";

export class FacultiesRepository {

  private httpModule: HTTPModule;

  constructor(){
    this.httpModule = new HTTPModule();
  }

  public retrieveFaculties(): Promise<Array<Faculty>> {

    return new Promise<Array<Faculty>>((resolve, reject) => {
      this.httpModule.httpGet(uniLeedsFacultiesURL)
        .then((result : string) => {
          this.parseFaculties(result)
            .then((facultyArr: Array<Faculty>) => {
              resolve(facultyArr);
            }).catch((error: any) => {
            reject(error);
          });
        }).catch((error: any) => {
        reject(error);
      });
    });
  }

  private parseFaculties(resp: string): Promise<Array<Faculty>>{
    return new Promise<Array<Faculty>>((resolve, reject) => {

      let faculties : Array<Faculty> = [];

      const dom = new JSDOM(resp);
      const facultiesRoot = dom.window.document.querySelector("div.faculty-container");
      const facultiesDom = facultiesRoot && facultiesRoot.querySelectorAll(".faculty");

      if(facultiesDom){
        for(let i=0; i<facultiesDom.length; i++){
          let NAME = '';
          let URL = '';

          const faculty_item = facultiesDom[i];

          const ps = faculty_item.querySelectorAll("p");

          let infos : Array<KeyValueUrl> = [];
          for(let j=0; j<ps.length; j++){
            const p_item = ps[j];
            // Always the first P tag has the name and URL of the faculty
            if(j == 0){
              const nameUrl = this.getNameAndUrl(p_item);
              NAME = nameUrl[0];
              URL = nameUrl[1];
            }else{
              // The rest P tags hold extra information about the faculty
              const kvu: KeyValueUrl | boolean = this.getKetValueUrl(p_item);
              if(kvu instanceof KeyValueUrl){
                infos.push(kvu);
              }

            }
          }

          faculties.push(new Faculty(NAME, URL, infos));
        }
      }

      resolve(faculties);
    });
  }

  private getKetValueUrl(p_item: any): KeyValueUrl | boolean {
    const strong = p_item.querySelector("strong");
    const a = p_item.querySelector("a");

    if(strong && a){
      var KEY = strong.innerHTML;
      var VALUE = a.innerHTML;

      const a_extra = strong.querySelector("a");
      if(a_extra){
        KEY = a_extra.innerHTML;
      }

      const strong_extra = a.querySelector("strong");
      if (strong_extra){
        VALUE = strong_extra.innerHTML;
      }

      const URL = a.getAttribute("href");

      return new KeyValueUrl(KEY,VALUE, URL);
    }else{
      return false;
    }
  }

  private getNameAndUrl(p_item: any): Array<string> {
    const a = p_item.querySelector("a");
    const URL = a && a.getAttribute("href");

    const strong = a && a.querySelector("strong");

    let NAME = "";
    if(strong == null){
      const strong2 = p_item.querySelector("strong");
      const a2 = strong2 && strong2.querySelector("a");
      NAME = a2 && a2.innerHTML || "";
    }else{
      NAME = strong.innerHTML;
    }
    NAME = NAME.replace("Visit the ","");
    NAME = NAME.replace("website","");
    NAME = NAME.replace("<br>","");

    return [NAME, URL];
  }

}
