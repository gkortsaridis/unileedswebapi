import {HTTPModule} from '../modules/HTTPModule';
import {JSDOM} from 'jsdom';
import {New} from "../entities/New";
import {superReplace, superTrim} from "../utilities/Helpers"
import {uniLeedsBaseURL} from "../utilities/Urls";

export class NewsRepository {

  private httpModule: HTTPModule;

  constructor(){
    this.httpModule = new HTTPModule();
  }

  public retrieveNews(): Promise<Array<New>>{
    return new Promise<Array<New>>((resolve, reject) => {


      this.httpModule.httpGet(uniLeedsBaseURL)
        .then((result : string) => {
          this.parseNews(result)
            .then((newsArr: Array<New>) => {
              resolve(newsArr);
            }).catch((error: any) => {
              reject(error);
          });
        }).catch((error: any) => {
          reject(error);
        });
    });

  }

  private parseNews(resp: string): Promise<Array<New>>{
    return new Promise<Array<New>>((resolve, reject) => {

      const dom = new JSDOM(resp);
      const newsRoot = dom.window.document.querySelector("div.news-grid__body");
      const tabNews = newsRoot && newsRoot.querySelector("#tabNews");
      const ajaxNews = tabNews && tabNews.querySelector("#ajaxNews");
      const news = ajaxNews && ajaxNews.querySelectorAll(".grid__item");

      let newsArr : Array<New> = [];

      if (news) {
        for (let i = 0; i < news.length; i++) {
          const news_item = news[i];

          const square = news_item.querySelector(".square");
          const span = square && square.querySelector("span");
          let IMG_URL = span && span.getAttribute("style");
          if (IMG_URL) {
            IMG_URL = IMG_URL.replace("background-image: url('//", "");
            IMG_URL = IMG_URL.replace("');", "");
          }
          const square_inner = square && square.querySelector(".square__inner");
          const content = square_inner && square_inner.querySelector(".square__inner__content");
          const a = square_inner && square_inner.querySelector("a");
          let URL = a && a.getAttribute("href");
          if (URL) {
            URL = URL.replace("//", "");
          }

          const details = content && content.querySelector(".square-details");
          const CATEGORY = details && details.querySelector("span");
          const DATETIME = details && details.querySelector("time");
          const TITLE = content && content.querySelector("h3");


          const newItem = new New(
            TITLE && TITLE.innerHTML || '',
            superTrim(CATEGORY && CATEGORY.innerHTML) || '',
            superTrim(DATETIME && DATETIME.getAttribute("datetime")) || '',
            superTrim(IMG_URL) || '',
            superTrim(URL) || ''
          );
          newsArr.push(newItem);
        }
        resolve(newsArr);
      }else{
        reject('No Parsed');
      }

    });

  }




}

