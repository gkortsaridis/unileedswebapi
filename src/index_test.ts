import {TimetableRepository} from "./repositories/TimetableRepository";
import {TimetableEntry} from "./entities/TimetableEntry";
import {Faculty} from "./entities/Faculty";
import {MongoDBRepository} from "./repositories/MongoDBRepository";
import {NewsRepository} from "./repositories/NewsRepository";
import {EventsRepository} from "./repositories/EventsRepository";
import {FacultiesRepository} from "./repositories/FacultiesRepository";
import {Event} from "./entities/Event";
import {New} from "./entities/New";
import {Module} from "./entities/Module";

const timetableRepository: TimetableRepository = new TimetableRepository();
const newsRepository: NewsRepository = new NewsRepository();
const eventsRepository: EventsRepository = new EventsRepository();
const facultiesRepository: FacultiesRepository = new FacultiesRepository();
const mongoDBRepository: MongoDBRepository = new MongoDBRepository();


facultiesRepository.retrieveFaculties().then((faculties: Array<Faculty>) => {
  console.log(faculties[4]);
});

